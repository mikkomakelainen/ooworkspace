package application;

import java.util.ArrayList;
import java.util.List;

public class TheatreAreas {
	private List<Areas> areas;
	private String Name;
	private String ID;

	public TheatreAreas() {
		areas = new ArrayList<Areas>();
	}


	public void AddTheater(String Name, String ID){
		Areas area = new Areas(Name, ID);
		areas.add(area);
	}

	public List<Areas> getAreas() {
		return areas;
	}


}

class Areas {
	private String Name;
	private String ID;

	public Areas(String Name, String ID) {
		this.Name = Name;
		this.ID = ID;
	}


	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String getId() {
		return ID;
	}

	public void setId(String ID) {
		this.ID = ID;
	}

}
