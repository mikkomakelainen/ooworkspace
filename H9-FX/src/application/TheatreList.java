package application;

import java.util.ArrayList;
import java.util.List;

public class TheatreList {
	private List<Theatre> theatres;

	public TheatreList() {
		theatres = new ArrayList<Theatre>();
	}


	public void AddTheater(String Name, String ID){
		Theatre theatre = new Theatre(Name, ID);
		theatres.add(theatre);
	}

	public List<Theatre> getAreas() {
		return theatres;
	}


}

class Theatre {
	private String Name;
	private String ID;

	public Theatre(String Name, String ID) {
		this.Name = Name;
		this.ID = ID;
	}


	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String getId() {
		return ID;
	}

	public void setId(String ID) {
		this.ID = ID;
	}

}
