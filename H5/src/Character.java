/*
 * Olio-ohjelmointi
 * Harjoitus 5 - 5
 * Mikko M�kel�inen
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Character {
	String name = "";
	String weaponName = "";
	
	public Character() {
		String s1 ="";
		while(name == "") {
			System.out.println("Valitse hahmosi: ");
			System.out.println("1) Kuningas");
			System.out.println("2) Ritari");
			System.out.println("3) Kuningatar");
			System.out.println("4) Peikko");
			
			System.out.print("Valintasi: ");
			
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				s1 = br.readLine();
			} catch (IOException ex) {
			}
			if(s1.equals("1")){
				King king = new King();
				name = king.getName();
			}else if(s1.equals("2")){
				Knight knight = new Knight();
				name = knight.getName();
			}else if(s1.equals("3")){
				Queen queen = new Queen();
				name = queen.getName();
			}else if(s1.equals("4")){
				Troll troll = new Troll();
				name = troll.getName();
			}else {
				System.out.println("V��r� valinta. Hyv�ksytt�v�t valinnat 1-4.");
			}
			if(name != "") {
				WeaponBehavior weapon = new WeaponBehavior(); 	
				weaponName = weapon.getName();
			}
		}
	}
	
	public String getName( ) {
		return name;
	}
	
	public String getWeaponName( ) {
		return weaponName;
	}
}

class King{
	String name = "King";
	
	public King() {
		
	}
	
	public String getName( ) {
		return name;
	}
}

class Knight{
	String name = "Knight";
	
	public Knight() {
		
	}
	
	public String getName( ) {
		return name;
	}
}

class Queen{
	String name = "Queen";
	
	public Queen() {
		
	}
	
	public String getName( ) {
		return name;
	}
}

class Troll{
	String name = "Troll";
	
	public Troll() {
		
	}
	
	public String getName( ) {
		return name;
	}
}

class WeaponBehavior{
	String weaponName = "";
	
	public WeaponBehavior() {
		String s1 = "";
		while(weaponName == "") {
			System.out.println("Valitse aseesi: ");
			System.out.println("1) Veitsi");
			System.out.println("2) Kirves");
			System.out.println("3) Miekka");
			System.out.println("4) Nuija");
			
			System.out.print("Valintasi: ");
			
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				s1 = br.readLine();
			} catch (IOException ex) {
			}
			if(s1.equals("1")){
				KnifeBehavior knife = new KnifeBehavior();
				weaponName = knife.getName();
			}else if(s1.equals("2")){
				AxeBehavior axe = new AxeBehavior();
				weaponName = axe.getName();
			}else if(s1.equals("3")){
				SwordBehavior sword = new SwordBehavior();
				weaponName = sword.getName();
			}else if(s1.equals("4")){
				ClubBehavior club = new ClubBehavior();
				weaponName = club.getName();
			}else {
				System.out.println("V��r� valinta. Hyv�ksytt�v�t valinnat 1-4.");
			}
		}
	}
	
	public String getName( ) {
		return weaponName;
	}
}

class KnifeBehavior{
	String weaponName = "Knife";
	
	public KnifeBehavior() {
		
	}
	
	public String getName( ) {
		return weaponName;
	}
}

class AxeBehavior{
	String weaponName = "Axe";
	
	public AxeBehavior() {
		
	}
	
	public String getName( ) {
		return weaponName;
	}
}

class SwordBehavior{
	String weaponName = "Sword";
	
	public SwordBehavior() {
		
	}
	
	public String getName( ) {
		return weaponName;
	}
}

class ClubBehavior{
	String weaponName = "Club";
	
	public ClubBehavior() {
		
	}
	
	public String getName( ) {
		return weaponName;
	}
}