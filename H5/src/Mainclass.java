/*
 * Olio-ohjelmointi
 * Harjoitus 5 - 5
 * Mikko M�kel�inen
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Mainclass {

	public static void main(String[] args) {
		String s1 ="";
		String weaponName = "";
		String name = "";
		boolean on = true;
		
		while(on) {
			System.out.println("*** TAISTELUSIMULAATTORI ***");
			System.out.println("1) Luo hahmo");
			System.out.println("2) Taistele hahmolla");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				s1 = br.readLine();
			} catch (IOException ex) {
			}
			if(s1.equals("1")){
				Character hero = new Character();
				name = hero.getName();
				weaponName = hero.getWeaponName();
			}else if(s1.equals("2")){
				System.out.println(name+" tappelee aseella "+ weaponName);
			}else if(s1.equals("0")){
				on = false;
			}else {
				System.out.println("V��r� valinta. Hyv�ksytt�v�t valinnat 1-3.");
			}
		}
	}

}
