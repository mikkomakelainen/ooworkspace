/*
 * Olio-ohjelmointi
 * Harjoitus 5 - 4
 * Mikko Makelainen
 */
import java.util.ArrayList;

public class Car {
	ArrayList<Object> partList = new ArrayList<Object>();

	public Car() {
		Body newBody = new Body();
		partList.add(newBody);
		Chassis newChassis = new Chassis();
		partList.add(newChassis);
		Engine newEngine = new Engine();
		partList.add(newEngine);
		Wheel wheel1 = new Wheel();
		Wheel wheel2 = new Wheel();
		Wheel wheel3 = new Wheel();
		Wheel wheel4 = new Wheel();
		partList.add(wheel4);

	}

	public void print() {
		System.out.println("Autoon kuuluu:");
		int i = 0;
		for (Object o : partList){
			if(i<3){
				System.out.println("\t"+o.getClass().getName());
				i++;
			}else{
				System.out.println("\t4 Wheel");
			}


		}
	}

}



class Chassis {
	public Chassis(){
		System.out.println("Valmistetaan: Chassis");
	}
}

class Body{
	public Body(){
		System.out.println("Valmistetaan: Body");
	}
}

class Engine{
	public Engine(){
		System.out.println("Valmistetaan: Engine");
	}
}

class Wheel{
	public Wheel(){
		System.out.println("Valmistetaan: Wheel");
	}
}
