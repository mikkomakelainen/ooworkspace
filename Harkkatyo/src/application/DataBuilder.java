package application;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DataBuilder {
	private static DataBuilder instance = null;
	public ArrayList <SmartPost> smartPosts;


	//When running the constructor for the first time create new arrayList and download XML data
    private DataBuilder() {
    	smartPosts = new ArrayList<SmartPost>();
    	readXML();
    }

    public static DataBuilder getInstance() {
    	if(instance == null) {
    		instance = new DataBuilder();
    	}
    	return instance;
    }
    
    public void readXML() {
		try{
			//Connect to SmartPost data
			URL url = new URL("http://smartpost.ee/fi_apt.xml");
			URLConnection conn = url.openConnection();
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(conn.getInputStream());
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("place");

			//Go through the DATA and save each SmartPost postcode, city, address, opening times, name, latitude and longitude to their own objects
			for(int i = 0; i < nList.getLength(); i++){
				Node nNode = nList.item(i);
				Element eElement = (Element) nNode;
				
				String code = eElement.getElementsByTagName("code").item(0).getTextContent();
				int postCode = Integer.parseInt(code);
				String city = eElement.getElementsByTagName("city").item(0).getTextContent();
				String address = eElement.getElementsByTagName("address").item(0).getTextContent();
				String openTimes = eElement.getElementsByTagName("availability").item(0).getTextContent();
				String postOffice = eElement.getElementsByTagName("postoffice").item(0).getTextContent();
				String lat = eElement.getElementsByTagName("lat").item(0).getTextContent();
				//Double latitude = Double.parseDouble(lat);
				String lng = eElement.getElementsByTagName("lng").item(0).getTextContent();
				//Double longitude = Double.parseDouble(lng);
				
				//Create new SmartPost object for each post and add it to the arraylist
				SmartPost smartPost = new SmartPost(postCode, city, address, openTimes, postOffice, lat, lng);
		    	smartPosts.add(smartPost);
			}
		}catch (Exception e){}	
	}
    
    public ArrayList<SmartPost> getSmartPost(){
    	return smartPosts;
    }
}
