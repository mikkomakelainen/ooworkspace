package application;

public abstract class Package {
	//This abstract class is used by the following classes: FirstClass, SecondClass and ThirdClass
	
	String objectName;
	int cubicSize;
	int mass;
	boolean breakable;
	SmartPost sendLocation;
	SmartPost sendDestination;
	String feature;
	
	String getName() {
		return objectName;
	}
	
	int getCubicSize() {
		return cubicSize;
	}
	
	int getMass() {
		return mass;
	}
	
	boolean getBreakable() {
		return breakable;
	}
	
	SmartPost getSendLocation() {
		return sendLocation;
	}
	
	SmartPost getSendDestination() {
		return sendDestination;
	}

	String getFeature() {
		return feature;
	}
}
