package application;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;

import javafx.event.ActionEvent;

import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;

public class MainPageController {
	@FXML
    private TextArea automatInfo;
	@FXML
	private WebView browser;
	@FXML
	private ComboBox<String> automatCombo;
	@FXML
    private ComboBox<String> packageListCombo;
	@FXML
	private Button addAutomatButton;
	@FXML
	private Button newPackageButton;
	@FXML
	private Button removeRouteButton;
	@FXML
	private Button sendPackageButton;


	//Initialize certain values and get instances of DataBuilder and Storage
	WebEngine webEngine;
	boolean newPackage = false;
	private DataBuilder dataList = DataBuilder.getInstance();
	private Storage storage = Storage.getInstance();


	@FXML
	void initialize() {

		//Start webview with index.html
		try {
			String goTo = new File("index.html").toURI().toURL().toExternalForm();
			webEngine = browser.getEngine();
			webEngine.load(goTo);
		} catch (Exception e) {}

		//Get SmartPost XML-Data
		ArrayList <SmartPost> smartPosts = dataList.getSmartPost();

		//Setup COMBOBOX for SmartPosts
		automatCombo.setPromptText("Valitse SmartPost-automaatti");
		for(int i = 0; i < smartPosts.size(); i++) {
			automatCombo.getItems().add(i+1+". "+smartPosts.get(i).getCity() +", "+smartPosts.get(i).getAddress());
    	}
	}

	/* Event Listener on ComboBox[#automatCombo].onAction
	 * This function shows information from selected smart post in a textarea
	 * */
	@FXML
	public void showInfo(ActionEvent event) {
		
		String selectedPost = automatCombo.getValue();
		//If selectedPost is empty or null stop the function
		if(selectedPost == "" || selectedPost == null)
			return;
		
		//Get the number of the selected smart post
		int number = Integer.parseInt(selectedPost.split("\\.")[0]) -1;

		//Get SmartPost XML-Data
		ArrayList <SmartPost> smartPosts = dataList.getSmartPost();
		SmartPost smartPost = smartPosts.get(number);

		//Display information from selected SmartPost
		automatInfo.setText(smartPost.getPostOffice()+"\n"+smartPost.getOpenTimes());
	}

	/* Event Listener on Button[#addAutomatButton].onAction
	 * This function adds selected smart post to the map via JavaScript
	 */
	@FXML
	public void addAutomat(ActionEvent event) {
		
		String selectedPost = automatCombo.getValue();
		//If selectedPost is empty or null stop the function
		if(selectedPost == "" || selectedPost == null)
			return;
		
		//Get the number of the selected smart post
		int number = Integer.parseInt(selectedPost.split("\\.")[0]) -1;

		//Get SmartPost XML-Data
		ArrayList <SmartPost> smartPosts = dataList.getSmartPost();
		SmartPost smartPost = smartPosts.get(number);
		String info = smartPost.getPostOffice()+'/'+smartPost.getAddress()+'/'+smartPost.getPostCode()+'/'+smartPost.getCity()+'/'+smartPost.getOpenTimes();

		//Execute JavaScript to show the post office marker on the map
		webEngine.executeScript("document.goToLocation('"+smartPost.getAddress()+", "+smartPost.getPostCode()+' '+smartPost.getCity()+"', '"+info+"', 'red')");
	}

	
	/* Event Listener on Button[#newPackageButton].onAction
	 * This function opens a new window (if one is not already open).
	 * The window is used to make new packages
	 */
	@FXML
	public void createPackage(ActionEvent event) {
		automatInfo.setText("");
		
		//Open a new window	if a window is not already opened
		if(!newPackage) {
			try {
				newPackage = true;
				Stage stage = new Stage();;
				Parent root = FXMLLoader.load(getClass().getResource("CreatePackage.fxml"));

				Scene scene = new Scene(root);
				scene.getStylesheets().addAll(this.getClass().getResource("application.css").toExternalForm());

				stage.setTitle("SmartPost-ohjelma: Uusi paketti");
				
				/*When the windows is closed refresh package list at packageListCombo and
				  set the boolean value	checking if a window is open to false	*/
				stage.setOnHidden(e -> {
					refreshPackageList(event);
					newPackage = false;
				});
				
				stage.setScene(scene);
				stage.show();
				stage.setAlwaysOnTop(true);

			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	/*This function refreshes comboBox named packageListCombo
	 * with the currently storaged <Package>s
	 */
	@FXML
    void refreshPackageList(ActionEvent event) {
		
		//Get all current packages from the storage
		ArrayList<Package> storageList = storage.getPackageStorage();
		
		//Clear the current comboBox
		packageListCombo.getItems().clear();
		
		//Go through packages and add them to the comboBox
		for(int i = 0; i < storageList.size(); i++) {
			packageListCombo.getItems().add(i+1+". "+storageList.get(i).getName());
		}
    }

	/* Event Listener on ComboBox[#packageListCombo].onAction
	 * This function shows information from selected package in a textarea
	 */
	@FXML
    void showPackageInfo(ActionEvent event) {
		
		String currentPackage = packageListCombo.getValue();
		//If currentPackage is empty or null stop the function
		if(currentPackage == "" || currentPackage == null)
			return;

		//Get the number of the selected package
		int number = Integer.parseInt(currentPackage.split("\\.")[0]) -1;

		//Get the information from the storage and add it to the textArea
		ArrayList<Package> storageList = storage.getPackageStorage();
		automatInfo.setText("Nimi: "+storageList.get(number).getName()+
				"\nKoko: "+storageList.get(number).getCubicSize()+
				" cm3\nPaino: "+storageList.get(number).getMass()+
				" kg\nOminaisuus: "+storageList.get(number).getFeature()+
				"\nReitti: "+storageList.get(number).getSendLocation().getCity()+" -> "+storageList.get(number).getSendDestination().getCity());
    }

	/* Event Listener on Button[#removeRouteButton].onAction
	 * This function removes all markers and routes from the map via JavaScript
	 */
	@FXML
	public void removeRoute(ActionEvent event) {
		webEngine.executeScript("document.deletePaths()");
	}

	/* Event Listener on Button[#sendPackageButton].onAction
	 * This function sends the selected package via JavaScript
	 */
	@FXML
	public void sendPackage(ActionEvent event) {
		
		String selectedPackage = packageListCombo.getValue();
		//If selectedPackage is empty or null stop the function
		if(selectedPackage == "" || selectedPackage == null)
			return;
		
		//Get the number of the selected package
		int number = Integer.parseInt(selectedPackage.split("\\.")[0]) -1;

		//Get the needed information for the package from the storage
		ArrayList<Package> storageList = storage.getPackageStorage();
		Package send = storageList.get(number);
		SmartPost sendLocation = send.getSendLocation();
		SmartPost sendDestination = send.getSendDestination();

		//Give the package certain variables depending on the packages class (first,second,third)
		String color = null, startColor = null;
		int speed = 0;
		String className = send.getClass().getSimpleName();
		if(className.equals("FirstClass")){
			speed = 1;
			color = "blue";
			startColor = "5F9EA0";
		}else if(className.equals("SecondClass")){
			speed = 2;
			color = "red";
			startColor = "C71585";
		}else if(className.equals("ThirdClass")){
			speed = 3;
			color = "green";
			startColor = "00FA9A";
		}

		//Start drawing the route
		webEngine.executeScript("document.createPath('"+sendLocation.getLatitude()+"', '"+sendLocation.getLongitude()+"', '"+sendDestination.getLatitude()+"', '"+sendDestination.getLongitude()+"', '"+color+"','"+speed+"')");

		//Get popup information on the start and end smart posts
		String infoLocation = sendLocation.getPostOffice()+'/'+sendLocation.getAddress()+'/'+sendLocation.getPostCode()+'/'+sendLocation.getCity()+'/'+sendLocation.getOpenTimes();
		String infoDestination = sendDestination.getPostOffice()+'/'+sendDestination.getAddress()+'/'+sendDestination.getPostCode()+'/'+sendDestination.getCity()+'/'+sendDestination.getOpenTimes();

		/*Add markers on route's start and end.
		 * Start marker is a custom marker with custom color and letter 'A' = 'Alku'.
		 * Using the same special marker for start and end didn't work flawlessly: after few tries both markers
		 * would show up as the last loaded one. After zooming in or out the markers would show up as the right ones.
		 * End marker is a dotless version of the basic marker with the route color.
		 */
		webEngine.executeScript("document.goToRouteLocation('"+sendLocation.getAddress()+", "+sendLocation.getPostCode()+' '+sendLocation.getCity()+"', '"+infoLocation+"', '"+startColor+"', 'A')");
		webEngine.executeScript("document.goToRouteLocation2('"+sendDestination.getAddress()+", "+sendDestination.getPostCode()+' '+sendDestination.getCity()+"', '"+infoDestination+"', '"+color+"')");
	}
}
