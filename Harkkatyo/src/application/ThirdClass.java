package application;

public class ThirdClass extends Package {
	//This class extends Package-class and uses it's defined variables and functions
	
	public ThirdClass() {

	}

	//This function creates a new gasoline package
	public void packageGasoline(SmartPost sendLocation, SmartPost sendDestination) {
		Gasoline gasoline = new Gasoline();

		this.objectName = "Bensiini";
		this.cubicSize = 10*20*60;
		this.mass = 15;
		this.breakable = false;
		this.sendLocation = sendLocation;
		this.sendDestination = sendDestination;
		this.feature = gasoline.getFeature();
	}

	//This function creates a new cement package
	public void packageCement(SmartPost sendLocation, SmartPost sendDestination) {
		Cement cement = new Cement();

		this.objectName = "Sementti";
		this.cubicSize = 10*36*60;
		this.mass = 20;
		this.breakable = false;
		this.sendLocation = sendLocation;
		this.sendDestination = sendDestination;
		this.feature = cement.getFeature();
	}

	//This function creates a new custom 3rd class package
	public void packageNewItem(String name, int size, int mass, boolean breakable, SmartPost sendLocation, SmartPost sendDestination) {
		this.objectName = name;
		this.cubicSize = size;
		this.mass = mass;
		this.breakable = breakable;
		this.sendLocation = sendLocation;
		this.sendDestination = sendDestination;
		this.feature = "Oma paketti";
	}
}
