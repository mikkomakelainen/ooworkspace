package application;

public class FirstClass extends Package {
	//This class extends Package-class and uses it's defined variables and functions
	
	public FirstClass() {

	}

	//This function creates a new screwbox package
	public void packageScrewbox(SmartPost sendLocation, SmartPost sendDestination) {
		Screwbox screwBox = new Screwbox();

		this.objectName = "Ruuvilaatikko";
		this.cubicSize = 20*20*40;
		this.mass = 10;
		this.breakable = false;
		this.sendLocation = sendLocation;
		this.sendDestination = sendDestination;
		this.feature = screwBox.getFeature();
	}

	//This function crease a new custom 1st class package
	public void packageNewItem(String name, int size, int mass, boolean breakable, SmartPost sendLocation, SmartPost sendDestination) {
		this.objectName = name;
		this.cubicSize = size;
		this.mass = mass;
		this.breakable = breakable;
		this.sendLocation = sendLocation;
		this.sendDestination = sendDestination;
		this.feature = "Oma paketti";
	}

}
