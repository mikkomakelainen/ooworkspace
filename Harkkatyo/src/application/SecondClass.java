package application;

public class SecondClass extends Package {
	//This class extends Package-class and uses it's defined variables and functions
	
	public SecondClass() {

	}

	//This function creates a new glasspanel package
	public void packageGlasspanel(SmartPost sendLocation, SmartPost sendDestination) {
		Glasspanel glasspanel = new Glasspanel();

		this.objectName = "Lasiruutu";
		this.cubicSize = 5*20*60;
		this.mass = 5;
		this.breakable = true;
		this.sendLocation = sendLocation;
		this.sendDestination = sendDestination;
		this.feature = glasspanel.getFeature();
	}

	//This function creates a new custom 2nd class package
	public void packageNewItem(String name, int size, int mass, boolean breakable, SmartPost sendLocation, SmartPost sendDestination) {
		this.objectName = name;
		this.cubicSize = size;
		this.mass = mass;
		this.breakable = breakable;
		this.sendLocation = sendLocation;
		this.sendDestination = sendDestination;
		this.feature = "Oma paketti";
		
	}

}
