package application;

import java.util.ArrayList;

public class Storage {
	private static Storage instance = null;
	public ArrayList <Package> packageStorage = new ArrayList<Package>();


    private Storage() {
    }

    public static Storage getInstance() {
    	if(instance == null) {
    		instance = new Storage();
    	}
    	return instance;
    }
    
    public ArrayList<Package> getPackageStorage(){
    	return packageStorage;
    }
    
    public void addPackage(Package input) {
    	packageStorage.add(input);
    }
}
