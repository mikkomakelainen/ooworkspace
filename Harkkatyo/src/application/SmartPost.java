package application;

public class SmartPost {
	private int postCode;
	private String city;
	private String address;
	private String openTimes;
	private String postOffice;
	private String latitude;
	private String longitude;
	
	//When creating a new SmartPost add the given inputs to the new SmartPost's variables
	public SmartPost(int postCode, String city, String address, String openTimes, String postOffice, String latitude, String longitude) {
		this.postCode = postCode;
		this.city = city;
		this.address = address;
		this.openTimes = openTimes;
		this.postOffice = postOffice;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public int getPostCode() {
		return postCode;
	}

	public String getCity() {
		return city;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getOpenTimes() {
		return openTimes;
	}
	
	public String getPostOffice() {
		return postOffice;
	}
	
	public String getLatitude() {
		return latitude;
	}
	
	public String getLongitude() {
		return longitude;
	}
}
