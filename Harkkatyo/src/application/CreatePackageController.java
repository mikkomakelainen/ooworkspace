package application;

import javafx.fxml.FXML;

import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;

import javafx.event.ActionEvent;

import javafx.scene.control.ComboBox;

import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.CheckBox;

public class CreatePackageController {
    @FXML
    private Text routeLength;
    @FXML
    private Text classOneInfo;
    @FXML
    private Text classTwoInfo;
    @FXML
    private Text classThreeInfo;
    @FXML
    private Text route;
	@FXML
	private ComboBox<String> objectComboBox;
	@FXML
	private WebView browser;
	@FXML
	private Button classInfo;
	@FXML
	private RadioButton classOne;
	@FXML
	private RadioButton classTwo;
	@FXML
	private RadioButton classThree;
	@FXML
	private CheckBox newObjectBreakable;
	@FXML
	private ComboBox<String> sendLocationCombo;
	@FXML
	private ComboBox<String> sendDestinationCombo;
	@FXML
	private Button createPackageButton;
	@FXML
	private Button cancelButton;
	@FXML
	private TextField newObjectName;
	@FXML
	private TextField newObjectSize;
	@FXML
	private TextField newObjectMass;
	@FXML
    private TextArea packageInfo;
	@FXML
    private TextArea errorText;

	//Initialize certain values and get instances of DataBuilder and Storage
	WebEngine webEngine;
	boolean textShown = false;
	private DataBuilder dataList = DataBuilder.getInstance();
	private Storage storage = Storage.getInstance();


	@FXML
	void initialize() {

		//Start a (hidden) webview with index.html. This is used to run JavaScript functions
		try {
			String goTo = new File("index.html").toURI().toURL().toExternalForm();
			webEngine = browser.getEngine();
			webEngine.load(goTo);
		} catch (Exception e) {}

		//Get SmartPost XML-Data
		ArrayList <SmartPost> smartPosts = dataList.getSmartPost();


		//Setup COMBOBOX for SmartPosts
		sendLocationCombo.setPromptText("Valitse SmartPost-automaatti");
		sendDestinationCombo.setPromptText("Valitse SmartPost-automaatti");
		for(int i = 0; i < smartPosts.size(); i++) {
			sendLocationCombo.getItems().add(i+1+". "+smartPosts.get(i).getCity() +", "+smartPosts.get(i).getAddress());
			sendDestinationCombo.getItems().add(i+1+". "+smartPosts.get(i).getCity() +", "+smartPosts.get(i).getAddress());
    	}
		
		//Setup COMBOBOX for different sendable objects
		objectComboBox.getItems().addAll("Esineet", "Ruuvilaatikko (1.lk)", "Lasiruutu (2. lk)", "Bensiini (3. lk)", "Laastia (3. lk)");
		
		
		sendDestinationCombo.setDisable(true);
		createPackageButton.setDisable(true);
	}


	/* Event Listener on ComboBox[#objectComboBox].onAction
	 * This function shows and disables certain features depending on the selected object
	 */
	@FXML
	public void objectSelect(ActionEvent event) {
		String comboBox = objectComboBox.getValue();

		/*If the custom object making object 'Esineet' is NOT selected all fields are disabled.
		 * If 'Esineet' IS selected the fields are enabled.
		 */
		if(!comboBox.equals("Esineet")) {
			classOne.setDisable(true);
			classTwo.setDisable(true);
			classThree.setDisable(true);
			newObjectName.setDisable(true);
			newObjectSize.setDisable(true);
			newObjectMass.setDisable(true);
			newObjectBreakable.setDisable(true);
		}else {
			classOne.setDisable(false);
			classTwo.setDisable(false);
			classThree.setDisable(false);
			newObjectName.setDisable(false);
			newObjectSize.setDisable(false);
			newObjectMass.setDisable(false);
			newObjectBreakable.setDisable(false);
			classOne.setSelected(false);
			classTwo.setSelected(false);
			classThree.setSelected(false);
			packageInfo.setVisible(false);
			packageInfo.setText("");
		}

		/*If a premade class is selected the right class on the radio buttons and
		 * show a textarea with information on the class
		 */
		if(comboBox.equals("Ruuvilaatikko (1.lk)")) {
			classOne.setSelected(true);
			classTwo.setSelected(false);
			classThree.setSelected(false);
			packageInfo.setVisible(true);
			packageInfo.setText("Koko: 20*20*40 \n\nPaino: 10 kg \n\nSarkyva: Ei \n\nHuomio: Kuljetus max 150 km");
		}else if(comboBox.equals("Lasiruutu (2. lk)")) {
			classOne.setSelected(false);
			classTwo.setSelected(true);
			classThree.setSelected(false);
			packageInfo.setVisible(true);
			packageInfo.setText("Koko: 5*20*60 \n\nPaino: 5 kg \n\nSarkyva: Kylla");
		}else if(comboBox.equals("Bensiini (3. lk)")) {
			classOne.setSelected(false);
			classTwo.setSelected(false);
			classThree.setSelected(true);
			packageInfo.setVisible(true);
			packageInfo.setText("Koko: 10*20*60 \n\nPaino: 15 kg \n\nSarkyva: Ei");
		}else if(comboBox.equals("Laastia (3. lk)")) {
			classOne.setSelected(false);
			classTwo.setSelected(false);
			classThree.setSelected(true);
			packageInfo.setVisible(true);
			packageInfo.setText("Koko: 10*36*60 \n\nPaino: 20 kg \n\nSarkyva: Ei");
		}
	}

	/* Event Listener on ComboBox[#objectComboBox].onAction
	 * This function calculates the distance between two smart posts via JavaScript.
	 * The distance is shown on a textarea. Shows errorMessage if first class package
	 * is selected while the distance is over 150.
	 */
	@FXML
    void automatSelect(ActionEvent event) {
		
		//Enable destination comboBox after selecting send location
		if(sendLocationCombo.getValue() != null) {
			sendDestinationCombo.setDisable(false);
		}

		//If both send location and destination is selected calculate the distance and show it
		if(sendLocationCombo.getValue() != null && sendDestinationCombo.getValue() != null) {
			
			if(createPackageButton.isDisabled()) {
				createPackageButton.setDisable(false);
			}
			
			//Get the information needed to calculate the distance
			String loc = sendLocationCombo.getValue();
			String dest = sendDestinationCombo.getValue();
			int locNumber = Integer.parseInt(loc.split("\\.")[0]) -1;
			int destNumber = Integer.parseInt(dest.split("\\.")[0]) -1;

			ArrayList <SmartPost> smartPosts = dataList.getSmartPost();
			String startLatitude = smartPosts.get(locNumber).getLatitude();
			String startLongitude = smartPosts.get(locNumber).getLongitude();
			String endLatitude = smartPosts.get(destNumber).getLatitude();
			String endLongitude = smartPosts.get(destNumber).getLongitude();
			Double returnV = null;

			/* Run JavaScript with the right values and get the distance as a return value.
			 * Depending on the return value Java might interpret it incorrectly as a INTEGER while it is always DOUBLE.
			 * Try-Catch is used to bypass this problem.
			 */
			try {
				returnV = (Double) webEngine.executeScript("document.routeLength('"+startLatitude+"', '"+startLongitude+"', '"+endLatitude+"', '"+endLongitude+"')");
			} catch(ClassCastException e){
				System.out.println("Double parsiminen ei toimi");
				returnV = Double.parseDouble(Integer.toString((Integer)webEngine.executeScript("document.routeLength('"+startLatitude+"', '"+startLongitude+"', '"+endLatitude+"', '"+endLongitude+"')")));
			}

			/* If the route is over 150 check selected class values and show error message.
			 * Depending on the selected sendable object enable/disable class one button.
			 */
			if(returnV > 150) {
				if(objectComboBox.getValue() != null) {
					if(objectComboBox.getValue().equals("Ruuvilaatikko (1.lk)")) {
						errorText.setVisible(true);
						errorText.setText("Valittuna liian pitka matka 1. luokalle");
					}
				}else if(classOne.isSelected()) {
					classOne.setDisable(true);
					classOne.setSelected(false);
					errorText.setVisible(true);
					errorText.setText("Valittuna liian pitka matka 1. luokalle");
				}
			}else {
				if(objectComboBox.getValue() != null) {
					if(objectComboBox.getValue().equals("Ruuvilaatikko (1.lk)")) {
						errorText.setVisible(false);
						errorText.setText("");
					}
				}else {
					classOne.setDisable(false);
					errorText.setVisible(false);
					errorText.setText("");
				}
			}
			
			//Show the current route and the route length on textareas
			route.setText(smartPosts.get(locNumber).getCity() + " - "+smartPosts.get(destNumber).getCity()+" : ");
			routeLength.setText(returnV+" km");
		}

    }

	/* Event Listener on Button[#classInfo].onAction
	 * This function shows/hides information text for different classes
	 */
	@FXML
	public void showClassInfo(ActionEvent event) {
		if(!textShown) {
			classOneInfo.setText("Max paino 10kg, koko 77 760 cm3, max 150 km reitti, nopein");
			classTwoInfo.setText("Max paino 5kg, koko 23 760 cm3, sarkyvien pakettien kuljetus");
			classThreeInfo.setText("Max paino 20kg, koko 127 440 cm3, hitain");
			textShown = true;
		}else {
			classOneInfo.setText("");
			classTwoInfo.setText("");
			classThreeInfo.setText("");
			textShown = false;
		}
	}
	
	/* Event Listener on RadioButton[#classOne]/[#classTwo]/[#classThree].onAction
	 * This function lets you only select one radio button
	 */
	@FXML
	public void classSelect(ActionEvent event) {
		
		if(classOne.isArmed()) {
			classTwo.setSelected(false);
			classThree.setSelected(false);
		}else if(classTwo.isArmed()) {
			classOne.setSelected(false);
			classThree.setSelected(false);
		}else if(classThree.isArmed()) {
			classOne.setSelected(false);
			classTwo.setSelected(false);
		}
	}

	/* Event Listener on Button[#createPackageButton].onAction
	 * This function checks selected information and creates a package if
	 * everything is correct. If there is a error it shows a error textarea
	 */
	@FXML
	public void createPackage(ActionEvent event) {
		
		//Initialize the error textarea
		errorText.setVisible(false);
		errorText.setText("");
		
		//Get the selected object, send location, send destination and list of smart posts 
		String comboBox = objectComboBox.getValue();
		String loc = sendLocationCombo.getValue();
		String dest = sendDestinationCombo.getValue();
		int locNumber = Integer.parseInt(loc.split("\\.")[0]) -1;
		int destNumber = Integer.parseInt(dest.split("\\.")[0]) -1;
		ArrayList <SmartPost> smartPosts = dataList.getSmartPost();

		/*
		 * Making custom objects
		 * Check if the custom object "Esineet" is selected (it is also the default shown value == null)
		 * 
		 */
		if(comboBox == null || comboBox.equals("Esineet")) {
			
			/* Get the given values from the fields.
			 * Check that user has given the custom object: name, size and mass.
			 * If not, show error message and stop the function.
			 */
		
			String name = newObjectName.getText();
			if(name.length() == 0) {
				errorText.setVisible(true);
				errorText.setText("Anna paketille nimi");
				return;
			}
			
			String sizeString = newObjectSize.getText();
			int size = 0;
			if(sizeString.length() == 0) {
				errorText.setVisible(true);
				errorText.setText("Anna paketille koko");
				return;
			}else {
				try {
					size = Integer.parseInt(sizeString);
				} catch(NumberFormatException e) {
					errorText.setVisible(true);
					errorText.setText("Anna paketin koko positiivisena\nkokonaislukuna");
					return;
				}
			}
			if(size <= 0) {
				errorText.setVisible(true);
				errorText.setText("Anna paketin koko positiivisena\nkokonaislukuna");
				return;
			}
			
			
			String massString = newObjectMass.getText();
			int mass = 0;
			if(massString.length() == 0) {
				errorText.setVisible(true);
				errorText.setText("Anna paketille paino");
				return;
			}else {	
				try {
					mass = Integer.parseInt(massString);
				} catch(NumberFormatException e) {
					errorText.setVisible(true);
					errorText.setText("Anna paketin paino positiivisena\nkokonaislukuna");
					return;
				}
			}
			if(mass <= 0) {
				errorText.setVisible(true);
				errorText.setText("Anna paketin paino positiivisena\nkokonaislukuna");
				return;
			}
			
			boolean breakable = newObjectBreakable.isSelected();
			SmartPost sendLocation = smartPosts.get(locNumber);
			SmartPost sendDestination = smartPosts.get(destNumber);
			
			if(classOne.isSelected()) {
				
				/* Check that the given size, mass, breakability and route length are appropriate.
				 * If not, show error message and stop the function.
				 */
				if(size > 77760) {
					errorText.setVisible(true);
					errorText.setText("Valittuna liian iso paketti 1. luokalle");
					return;
				}else if(mass > 10) {
					errorText.setVisible(true);
					errorText.setText("Valittuna liian painava paketti 1. luokalle");
					return;
				}else if(breakable) {
					errorText.setVisible(true);
					errorText.setText("Valittuna liian s�rkyv� paketti 1. luokalle");
					return;
				}
				
				int route= Integer.parseInt(routeLength.getText().split("\\.")[0]);
				if(route > 150) {
					errorText.setVisible(true);
					errorText.setText("Valittuna liian pitk� matka 1. luokalle");
					return;
				}
				
				//Create a new custom first class package and add it to the storage
				FirstClass firstClass = new FirstClass();
				firstClass.packageNewItem(name, size, mass, breakable, sendLocation, sendDestination);
				storage.addPackage(firstClass);
				
			}else if(classTwo.isSelected()) {
				
				/* Check that the given size and mass are appropriate.
				 * If not, show error message and stop the function.
				 */
				if(size > 23760) {
					errorText.setVisible(true);
					errorText.setText("Valittuna liian iso paketti 2. luokalle");
					return;
				}else if(mass > 5) {
					errorText.setVisible(true);
					errorText.setText("Valittuna liian painava paketti 2. luokalle");
					return;
				}
				
				//Create a new custom second class package and add it to the storage
				SecondClass secondClass = new SecondClass();
				secondClass.packageNewItem(name, size, mass, breakable, sendLocation, sendDestination);
				storage.addPackage(secondClass);
				
			}else if(classThree.isSelected()) {
				
				/* Check that the given size, mass and breakability are appropriate.
				 * If not, show error message and stop the function.
				 */
				if(size > 127440) {
					errorText.setVisible(true);
					errorText.setText("Valittuna liian iso paketti 3. luokalle");
					return;
				}else if(mass > 20) {
					errorText.setVisible(true);
					errorText.setText("Valittuna liian painava paketti 3. luokalle");
					return;
				}else if(breakable) {
					errorText.setVisible(true);
					errorText.setText("Valittuna liian s�rkyv� paketti 3. luokalle");
					return;
				}
				
				//Create a new custom third class package and add it to the storage
				ThirdClass thirdClass = new ThirdClass();
				thirdClass.packageNewItem(name, size, mass, breakable, sendLocation, sendDestination);
				storage.addPackage(thirdClass);
				
			}else {
				//Stop function and show error message if no class is selected
				
				errorText.setVisible(true);
				errorText.setText("Luokkaa ei ole valittuna");
				return;
			}
			
		/*
		 * 	Making premade objects
		 * 	Check what premade object is selected, create a new 1./2./3. class package depending
		 *  on the premade object and add it to the storage.
		 * 	If selected object is 1. class, check that the route length is not too long.
		 * 
		 */
		}else if(comboBox.equals("Ruuvilaatikko (1.lk)")) {
			int route= Integer.parseInt(routeLength.getText().split("\\.")[0]);
			if(route > 150) {
				errorText.setVisible(true);
				errorText.setText("Valittuna liian pitk� matka 1. luokalle");
				return;
			}
			
			FirstClass firstClass = new FirstClass();
			firstClass.packageScrewbox(smartPosts.get(locNumber), smartPosts.get(destNumber));
			storage.addPackage(firstClass);
		}else if(comboBox.equals("Lasiruutu (2. lk)")) {
			SecondClass secondClass = new SecondClass();
			secondClass.packageGlasspanel(smartPosts.get(locNumber), smartPosts.get(destNumber));
			storage.addPackage(secondClass);
		}else if(comboBox.equals("Bensiini (3. lk)")) {
			ThirdClass thirdClass = new ThirdClass();
			thirdClass.packageGasoline(smartPosts.get(locNumber), smartPosts.get(destNumber));
			storage.addPackage(thirdClass);
		}else if(comboBox.equals("Laastia (3. lk)")) {
			ThirdClass thirdClass = new ThirdClass();
			thirdClass.packageCement(smartPosts.get(locNumber), smartPosts.get(destNumber));
			storage.addPackage(thirdClass);
		}
		
		
		/* At this point a new object has been made and added to the storage.
		 * Close the stage.
		 */
		Stage stage = (Stage) createPackageButton.getScene().getWindow();
	    stage.close();
	}
	
	/* Event Listener on Button[#cancelButton].onAction
	 * This function closes the stage.
	 */
	@FXML
	public void cancel(ActionEvent event) {
		// TODO Autogenerated
		Stage stage = (Stage) cancelButton.getScene().getWindow();
	    stage.close();
	}
}
