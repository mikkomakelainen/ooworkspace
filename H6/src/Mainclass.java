/*
 * Olio-ohjelmointi
 * Harjoitus 6 - 5
 * Mikko M�kel�inen
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Mainclass {

	public static void main(String[] args) {
		boolean on = true;
		String s1 = "";
		String s2 = "";
		String s3 = "";
		Bank bank = new Bank();
		
		while(on) {
			System.out.println();
			System.out.println("*** PANKKIJ�RJESTELM� ***");
			System.out.println("1) Lis�� tavallinen tili");
			System.out.println("2) Lis�� luotollinen tili");
			System.out.println("3) Tallenna tilille rahaa");
			System.out.println("4) Nosta tililt�");
			System.out.println("5) Poista tili");
			System.out.println("6) Tulosta tili");
			System.out.println("7) Tulosta kaikki tilit");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				s1 = br.readLine();
			} catch (IOException ex) {
			}
			if(s1.equals("1")){		/*Lis�� tavallinen tili*/
				System.out.print("Sy�t� tilinumero: ");
				BufferedReader br2 = new BufferedReader(
						new InputStreamReader(System.in));
				try {
					s1 = br2.readLine();
				} catch (IOException ex) {
				}
				System.out.print("Sy�t� raham��r�: ");
				BufferedReader br3 = new BufferedReader(
						new InputStreamReader(System.in));
				try {
					s2 = br3.readLine();
				} catch (IOException ex) {
				}
				bank.newAccount(0, s1, Integer.parseInt(s2), 0);
			}else if(s1.equals("2")){	/*Lis�� luotollinen tili*/
				System.out.print("Sy�t� tilinumero: ");
				BufferedReader br2 = new BufferedReader(
						new InputStreamReader(System.in));
				try {
					s1 = br2.readLine();
				} catch (IOException ex) {
				}
				System.out.print("Sy�t� raham��r�: ");
				BufferedReader br3 = new BufferedReader(
						new InputStreamReader(System.in));
				try {
					s2 = br3.readLine();
				} catch (IOException ex) {
				}
				System.out.print("Sy�t� luottoraja: ");
				BufferedReader br4 = new BufferedReader(
						new InputStreamReader(System.in));
				try {
					s3 = br4.readLine();
				} catch (IOException ex) {
				}
				bank.newAccount(1, s1, Integer.parseInt(s2), Integer.parseInt(s3));
			}else if(s1.equals("3")){	/*Tallenna tilille rahaa*/
				System.out.print("Sy�t� tilinumero: ");
				BufferedReader br2 = new BufferedReader(
						new InputStreamReader(System.in));
				try {
					s1 = br2.readLine();
				} catch (IOException ex) {
				}
				System.out.print("Sy�t� raham��r�: ");
				BufferedReader br3 = new BufferedReader(
						new InputStreamReader(System.in));
				try {
					s2 = br3.readLine();
				} catch (IOException ex) {
				}
				bank.moveMoney(0, s1, Integer.parseInt(s2));
			}else if(s1.equals("4")){	/*Nosta tililt� rahaa*/
				System.out.print("Sy�t� tilinumero: ");
				BufferedReader br2 = new BufferedReader(
						new InputStreamReader(System.in));
				try {
					s1 = br2.readLine();
				} catch (IOException ex) {
				}
				System.out.print("Sy�t� raham��r�: ");
				BufferedReader br3 = new BufferedReader(
						new InputStreamReader(System.in));
				try {
					s2 = br3.readLine();
				} catch (IOException ex) {
				}
				bank.moveMoney(1, s1, Integer.parseInt(s2));
			}else if(s1.equals("5")){	/*Poista tili*/
				System.out.print("Sy�t� poistettava tilinumero: ");
				BufferedReader br2 = new BufferedReader(
						new InputStreamReader(System.in));
				try {
					s1 = br2.readLine();
				} catch (IOException ex) {
				}
				bank.removeAccount(s1);
			}else if(s1.equals("6")){	/*Tulosta tili*/
				System.out.print("Sy�t� tulostettava tilinumero: ");
				BufferedReader br2 = new BufferedReader(
						new InputStreamReader(System.in));
				try {
					s1 = br2.readLine();
				} catch (IOException ex) {
				}
				bank.findAccount(s1);
			}else if(s1.equals("7")){	/*Tulosta kaikki tilit*/
				bank.printAll();
			}else if(s1.equals("0")){	/*Lopeta*/
				on = false;
			}else {
				System.out.println("Valinta ei kelpaa.");
			}
		}
	}
}