/*
 * Olio-ohjelmointi
 * Harjoitus 6 - 5
 * Mikko M�kel�inen
 */

public abstract class Account{
	
	String accountNumber;
	int money;
	int credit;
	
	String getNumber() {
		return accountNumber;
	}

	int getMoney() {
		return money;
	}
	
	int getCredit() {
		return credit;
	}
	
	abstract void setMoney(int changeType, int change);
}


class BasicAccount extends Account {
	
	public BasicAccount(String s1, int i1) {
		this.accountNumber = s1;
		this.money = i1;
		System.out.println("Tili luotu.");
	}
	
	void setMoney(int changeType, int change) {
		if(changeType == 0) {
			money = money + change;
		}else {
			if((money - change) < 0) {
				System.out.println("Nosto ep�onnistui. Tilill� ei ole tarpeeksi katetta.");
				System.out.println("Tilin saldo: "+money);
			}else {
				money = money - change;
			}
		}
	}
}

class CreditAccount extends Account {
	
	public CreditAccount(String s1, int i1, int i2) {
		this.accountNumber = s1;
		this.money = i1;
		this.credit = i2;
		System.out.println("Tili luotu.");
	}
	
	void setMoney(int changeType, int change) {
		if(changeType == 0) {
			money = money + change;
		}else {
			if((money + credit - change) < 0) {
				System.out.println("Nosto ep�onnistui. Tilill� ei ole tarpeeksi luottoa.");
				System.out.println("Tilin saldo: "+money+" Luottoraja: "+credit);
			}else {
				money = money - change;
			}
		}
	}
}