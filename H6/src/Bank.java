/*
 * Olio-ohjelmointi
 * Harjoitus 6 - 5
 * Mikko M�kel�inen
 */

import java.util.ArrayList;

public class Bank {

	ArrayList<Account> accountList = new ArrayList<Account>();
	
	public Bank() {;
	}
	
	public void newAccount(int accountType, String accountNumber, int money, int credit) {
		if(accountType == 0) {
			BasicAccount newAcco = new BasicAccount(accountNumber, money);
			accountList.add(newAcco);
		}else {
			CreditAccount newAcco = new CreditAccount(accountNumber, money, credit);
			accountList.add(newAcco);
		}
	}	
	
	public void removeAccount(String accountNumber) {
		boolean find = false;
		for(Account o : accountList){
			if(o.getNumber().equals(accountNumber)) {
				accountList.remove(o);
				System.out.println("Tili poistettu.");
				find = true;
				break;
			}
		}
		if(!find) {
			System.out.println("Tili� "+ accountNumber+" ei l�ytynyt.");
		}
	}
	
	public void moveMoney(int moveType, String accountNumber, int moreMoney) {
		boolean find = false;
		if(moveType == 0) {
			for(Account o : accountList){
				if(o.getNumber().equals(accountNumber)) {
					o.setMoney(0, moreMoney);
					find = true;
				}
			}
		}else {
			for(Account o : accountList){
				if(o.getNumber().equals(accountNumber)) {
					o.setMoney(1, moreMoney);
					find = true;
				}
			}
		}
		if(!find) {
			System.out.println("Tili� "+ accountNumber+" ei l�ytynyt.");
		}
	}
	
	public void findAccount(String accountNumber) {
		boolean find = false;
		for(Account o : accountList){
			if(o.getNumber().equals(accountNumber)) {
				System.out.println("Tilinumero: "+ o.getNumber()+" Tilill� rahaa: "+o.getMoney());
				find = true;
				break;
			}
		}
		if(!find) {
			System.out.println("Tili� "+ accountNumber+" ei l�ytynyt.");	
		}
	}
	
	public void printAll() {
		if(accountList.size() == 0) {
			System.out.println("Yht��n tili� ei l�ytynyt.");
		}else {
			System.out.println("Kaikki tilit:");
			for(Account o : accountList){
				if(o.getClass().getName().equals("BasicAccount")) {
					System.out.println("Tilinumero: "+ o.getNumber()+" Tilill� rahaa: "+o.getMoney());
				}else {
					System.out.println("Tilinumero: "+ o.getNumber()+" Tilill� rahaa: "+o.getMoney()+" Luottoraja: "+o.getCredit());
				}
			}
		}
	}
}