package application;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.layout.AnchorPane;

public class Shapehandler{
	private static Shapehandler instance = null;
	private ArrayList<Point> shapes;
	private ArrayList<NewLine> lines;

    private Shapehandler() {
    	shapes = new ArrayList<Point>();
    	lines = new ArrayList<NewLine>();
    }


    public static Shapehandler getInstance() {
    	if(instance == null) {
    		instance = new Shapehandler();
    	}
    	return instance;
    }


	public void AddCircle(Double x, Double y, AnchorPane background){
		Point circle = new Point(x, y, background);
		shapes.add(circle);

	}

	public void AddLine(Double startX, Double startY, Double pointX, Double pointY, AnchorPane background){
		NewLine line = new NewLine(startX, startY, pointX, pointY, background);
		lines.add(line);

	}

	public List<Point> getShapes() {
		return shapes;
	}

}