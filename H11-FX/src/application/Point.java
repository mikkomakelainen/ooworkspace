package application;

import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class Point {
	private Double coordX;
	private Double coordY;

	public Point(Double x, Double y, AnchorPane background){
		this.coordX = x;
		this.coordY = y;
		Circle circle = new Circle(x,y,10.0);
		circle.setCache(true);
		background.getChildren().add(circle);
	}

	public Double getX() {
		return coordX;
	}

	public Double getY() {
		return coordY;
	}
}

class NewLine{
	private Line line;

	public NewLine(Double x1, Double y1, Double x2, Double y2, AnchorPane background){
		Line line = new Line(x1, y1, x2, y2);
		line.setCache(true);
		background.getChildren().add(line);
		this.line = line;
	}

	public Line returnLine() {
		return line;
	}

	public void removeLine(NewLine input) {

	}
}




