/*
 * Olio-ohjelmointi
 * Harjoitus 4 - 5
 * Mikko Makelainen
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ReadAndWriteIO {
	
	public void readFile(String input) throws FileNotFoundException, IOException {
		String s1;
		BufferedReader in = new BufferedReader(new FileReader(input));;
		while((s1 = in.readLine()) != null ) {
			System.out.println(s1);
		}
		in.close();
	}
	
	public void readZip(String input) throws FileNotFoundException, IOException {
		//Avataan zip
		try {
            ZipFile zip = new ZipFile(input);
            Enumeration<? extends ZipEntry> files = zip.entries();
 
            //K�yd��n zip:n tiedostoja l�pi
            while (files.hasMoreElements()) {
                ZipEntry entry = files.nextElement();
                String name = entry.getName();
                
                //Etsit��n .txt-tiedosto
                if(name.indexOf(".txt") >=0) {
                	String s1;
                	
                	//Luetaan zip-tiedostosta
                	InputStream zipInput = zip.getInputStream(entry);
            		BufferedReader in = new BufferedReader(new InputStreamReader(zipInput, "UTF-8"));;
            		
            		//Tulostetaan teksti
            		while((s1 = in.readLine()) != null ) {
            			System.out.println(s1);
            		}
            		in.close();
            	}
              }
            zip.close();
        } catch (IOException ex) {
        }
    }
	
	public void readAndWrite(String input, String output) throws FileNotFoundException, IOException {
		String s1;
		BufferedReader in = new BufferedReader(new FileReader(input));
		BufferedWriter out = new BufferedWriter(new FileWriter(output));
		while((s1 = in.readLine()) != null ) {
			out.write(s1);
			out.newLine();
		}
		in.close();
		out.close();
	}
	
	public void readAndWrite30(String input, String output) throws FileNotFoundException, IOException {
		String s1;
		BufferedReader in = new BufferedReader(new FileReader(input));
		BufferedWriter out = new BufferedWriter(new FileWriter(output));
		while((s1 = in.readLine()) != null) {
			if(s1.length() <30 && s1.trim().length() >0) {
				out.write(s1);
				out.newLine();
			}
		}
		in.close();
		out.close();
	}
	
	public void readAndWrite30v(String input, String output) throws FileNotFoundException, IOException {
		String s1;
		BufferedReader in = new BufferedReader(new FileReader(input));
		BufferedWriter out = new BufferedWriter(new FileWriter(output));
		while((s1 = in.readLine()) != null) {
			if(s1.length() <30 && s1.trim().length() >0 && s1.indexOf('v') >=0) {
				out.write(s1);
				out.newLine();
			}
		}
		in.close();
		out.close();
	}
}