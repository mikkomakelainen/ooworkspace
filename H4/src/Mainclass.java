/*
 * Olio-ohjelmointi
 * Harjoitus 4 - 5
 * Mikko Makelainen
 */

import java.io.FileNotFoundException;
import java.io.IOException;

public class Mainclass {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		ReadAndWriteIO IO = new ReadAndWriteIO(); 
		IO.readZip("zipinput.zip");
	}

}