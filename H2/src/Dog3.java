/*
 * Olio-ohjelmointi
 * Harjoitus 1 - 3
 * Mikko Mäkeläinen
 */

public class Dog3 {

	private String name;

	//Rakentaja
	public Dog3 (String dogName) {
		dogName = dogName.trim();
		if((dogName.isEmpty()) || (dogName.equals(" "))) {
			name = "Doge";
			dogName = name;
		}else{
			name = dogName;
		}
		System.out.println("Hei, nimeni on " + dogName +"!");
	}

	//Puhumismetodi
	public void Speak (String input) {
		input = input.trim();
		if((input.isEmpty()) || (input.equals(" "))) {
			input = "Much wow!";
		}
		System.out.println(name +": " + input);
	}
}
