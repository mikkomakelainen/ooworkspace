/*
 * Olio-ohjelmointi
 * Harjoitus 1 - 2
 * Mikko Mäkeläinen
 */

public class Dog2 {

	String name;

	public Dog2 (String dogName) {
		name = dogName;
		System.out.println("Hei, nimeni on " + dogName +"!");
	}

	public void Speak (String input) {
		System.out.println(name +": " + input);
	}
}
