/*
 * Olio-ohjelmointi
 * Harjoitus 1 - 1
 * Mikko Mäkeläinen
 */

public class Dog {


	public Dog (String dogName) {
		System.out.println("Hei, nimeni on " + dogName +"!");
	}

	public void Speak (String input) {
		System.out.println(input);
	}
}
