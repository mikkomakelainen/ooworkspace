import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Mainclass5 extends Dog5 {

	public Mainclass5(String dogName) {
		super(dogName);
	}

	public static void main(String []args) {
		//Pääohjelma
		String s1 = "";
		String s2 = "";
		String s3 = "";
		Scanner scan = null;

		//Kysyy koiran nimen
		System.out.print("Anna koiralle nimi: ");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			s1 = br.readLine();
		} catch (IOException ex) {
		}

		Dog5 newDog = new Dog5(s1);
		
		//Kysyy mitä koira sanoo
		System.out.print("Mitä koira sanoo: ");
			scan = new Scanner(System.in);
			s3 = scan.nextLine();
			Scanner scan2 = new Scanner(s3);
			while(scan2.hasNext()) {
				if(scan2.hasNextBoolean() == true) {
					s2 = scan2.next();
					newDog.Speak(s2, "Boolean");
				}else if(scan2.hasNextInt()) {
					s2 = scan2.next();
					newDog.Speak(s2, "Integer");
				}else {
					s2 = scan2.next();
					newDog.Speak(s2, "Error");
				}
			}
			scan.close();
			scan2.close();
		}
	}