/*
 * Olio-ohjelmointi
 * Harjoitus 2 - 5
 * Mikko Mäkeläinen
 */

public class Dog5 {

	private String name;

	//Rakentaja
	public Dog5 (String dogName) {
		dogName = dogName.trim();
		if((dogName.isEmpty()) || (dogName.equals(" "))) {
			name = "Doge";
			dogName = name;
		}else{
			name = dogName;
		}
		System.out.println("Hei, nimeni on " + dogName);
	}

	//Puhumismetodi
	public void Speak (String input, String dataType) {
		input = input.trim();
		if((input.isEmpty()) || (input.equals(" "))) {
			input = "Much wow!";
		}
		
		if(dataType == "Boolean") {
			System.out.println("Such boolean: " + input);
		}else if (dataType == "Integer") {
			System.out.println("Such integer: " + input);
		}else {
			System.out.println(input);
		}
	}	
}
