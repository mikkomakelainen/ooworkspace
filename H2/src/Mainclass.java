import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Mainclass extends Dog3 {

	public Mainclass(String dogName) {
		super(dogName);
	}

	public static void main(String []args) {
		//Pääohjelma
		String s1 = "";
		String s2 = "";
		String s3 = "";

		//Kysyy koiran nimen
		System.out.print("Anna koiralle nimi: ");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			s1 = br.readLine();
		} catch (IOException ex) {
		}

		Dog3 newDog = new Dog3(s1);


		//Kysyy mitä koira sanoo
		System.out.print("Mitä koira sanoo: ");
		BufferedReader br2 = new BufferedReader(
				new InputStreamReader(System.in));
		try {
			s2 = br2.readLine();
		} catch (IOException ex) {
		}

		newDog.Speak(s2);

		s3 = s2;
		s3 = s3.trim();

		if((s3.isEmpty())) {
			System.out.print("Mitä koira sanoo: ");
			BufferedReader br3 = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				s2 = br3.readLine();
			} catch (IOException ex) {
			}

			newDog.Speak(s2);
		}
	}
}
