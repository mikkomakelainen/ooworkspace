package application;


public class Bottle {
	private String name;
	private String manufacturer;
	private double total_energy;
	private	double size;
	private double price;

	public Bottle(int arvo) {
		if(arvo == 0) {
			name = "Pepsi Max";
			manufacturer = "Pepsi";
			total_energy = 0.3;
			size = 0.5;
			price = 1.80;
		}else if(arvo == 1) {
			name = "Pepsi Max";
			manufacturer = "Pepsi";
			total_energy = 0.9;
			size = 1.5;
			price = 2.20;
		}else if(arvo == 2) {
			name = "Coca-Cola Zero";
			manufacturer = "Coca-Cola";
			total_energy = 0.1;
			size = 0.5;
			price = 2.0;
		}else if(arvo == 3) {
			name = "Coca-Cola Zero";
			manufacturer = "Coca-Cola";
			total_energy = 0.3;
			size = 1.5;
			price = 2.5;
		}else if(arvo == 4 || arvo == 5) {
			name = "Fanta Zero";
			manufacturer = "Coca-Cola";
			total_energy = 0.2;
			size = 0.5;
			price = 1.9;
		}
	}


	public String getName() {
		return name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public double getEnergy() {
		return total_energy;
	}

	public double getSize() {
		return size;
	}

	public double getPrice() {
		return price;
	}
}