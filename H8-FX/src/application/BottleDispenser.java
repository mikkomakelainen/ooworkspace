package application;


import java.util.ArrayList;


public class BottleDispenser {
	private static BottleDispenser instance = null;

	private double bottles;
    private double money;
    private ArrayList<Bottle> bottleList = new ArrayList<Bottle>();

    private BottleDispenser() {
    //Alustaa rahan ja pullojen maaran, luo listan pulloista
        bottles = 6;
        money = 0;
        for(int i = 0; i < bottles; i++) {
        	bottleList.add(new Bottle(i));
        }
    }

    public double getMoney(){
    	return Math.round(money * 10) / 10D;
    }

    public static BottleDispenser getInstance() {
    	if(instance == null) {
    		instance = new BottleDispenser();
    	}
    	return instance;
    }

    public void removeBottle(int choice) {
   //Poistaa pullolistasta pulloja
    	choice -= 1;
    	bottleList.remove(bottleList.remove(choice));
    }

    public ArrayList<Bottle> getList() {
    //Listaa saatavilla olevat tuotteet
   		return bottleList;
    }

    public void addMoney(double input) {
    //Lisaa rahaa yhden yksikon
        money += input;
    }

    public int buyBottle(int number) {
    /*Myy pullon, jos rahaa ja pulloja on tarpeeksi.
      Palauttaa myynnin onnistuessa 1, epaonnistuessa 0*/
    	number -= 1;
    	if(money < bottleList.get(number).getPrice()) {
    		return 0;
    	}else{
    		money -= bottleList.get(number).getPrice();
    		return 1;
    	}
    }

    public void returnMoney() {
    //Palauttaa loput rahat
    	String change = String.format("%.02f", money);
    	money = 0;
        System.out.println("Klink klink. Sinne menivatt rahat! Rahaa tuli ulos "+change+"e");
    }
}