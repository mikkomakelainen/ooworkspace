/*
 * Olio-ohjelmointi
 * Harjoitus 3 - 5
 * Mikko Makelainen
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Mainclass {


	public static void main(String []args) {
		BottleDispenser newBottle = new BottleDispenser();
		String s1 = "";
		String s2 = "";
		Integer choice;

		while(true) {
			newBottle.getUI();
			//Kysyy mit� valikosta valitaan
			System.out.print("Valintasi: ");
			BufferedReader br = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				s1 = br.readLine();
			} catch (IOException ex) {
			}
			
			//Lis�� rahaa
			if(s1.equals("1")) {
				newBottle.addMoney();
				
			//Osta pullo
			}else if(s1.equals("2")) {
				newBottle.getList();
				System.out.print("Valintasi: ");
				BufferedReader br2 = new BufferedReader(
						new InputStreamReader(System.in));
				try {
					s2 = br2.readLine();
				} catch (IOException ex) {
				}
				choice = Integer.parseInt(s2);
				if(newBottle.buyBottle(choice) == 1) {
					newBottle.removeBottle(choice);
				}
				
			//Palauta rahat
			}else if(s1.equals("3")) {
				newBottle.returnMoney();
				
			//Listaa ostettavat pullot
			}else if(s1.equals("4")) {
				newBottle.getList();
				
			//Lopeta ohjelma
			}else if(s1.equals("0")) {
				return;
			
			//Virheilmoitus
			}else {
				System.out.println("Virheellinen valinta. Valinnat 0-4.");
			}
		}
	}
}