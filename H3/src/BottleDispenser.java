/*
 * Olio-ohjelmointi
 * Harjoitus 3 - 5
 * Mikko Makelainen
 */

import java.util.ArrayList;


public class BottleDispenser {
	private int bottles;
    private double money;
    private ArrayList<Bottle> bottleList = new ArrayList<Bottle>();

    public BottleDispenser() {
    //Alustaa rahan ja pullojen m��r�n, luo listan pulloista
        bottles = 6;
        money = 0;
        for(int i = 0; i < bottles; i++) {
        	bottleList.add(new Bottle(i));
        }
    }
    
    public void removeBottle(int choice) {
   //Poistaa pullolistasta pulloja
    	choice -= 1;
    	bottleList.remove(bottleList.remove(choice));
    }
    
    public void getUI() {
    //Luo valikkorakenteen ohjelmalle
		System.out.print("\n");
		System.out.println("*** LIMSA-AUTOMAATTI ***");
		System.out.println("1) Lis�� rahaa koneeseen");
		System.out.println("2) Osta pullo");
		System.out.println("3) Ota rahat ulos");
		System.out.println("4) Listaa koneessa olevat pullot");
		System.out.println("0) Lopeta");
    }
    
    public void getList() {
    //Listaa saatavilla olevat tuotteet
    	for(int i = 0; i < bottleList.size(); i++) {
    		System.out.println((i+1)+". Nimi: "+ bottleList.get(i).getName());
    		System.out.println("\tKoko: "+bottleList.get(i).getSize()+"\tHinta: "+bottleList.get(i).getPrice());
    	}
    }

    public void addMoney() {
    //Lis�� rahaa yhden yksik�n
        money += 1;
        System.out.println("Klink! Lis�� rahaa laitteeseen!");
    }

    public int buyBottle(int number) {
    /*Myy pullon, jos rahaa ja pulloja on tarpeeksi.
      Palauttaa myynnin onnistuessa 1, ep�onnistuessa 0*/
    	number -= 1;
    	if(money < bottleList.get(number).getPrice()) {
    		System.out.println("Sy�t� rahaa ensin!");
    		return 0;
    	}else if(bottles == 0) {
    		System.out.println("Pullot ovat loppu!");
    		return 0;
    	}else{
    		bottles -= 1;
    		money -= bottleList.get(number).getPrice();
    		System.out.println("KACHUNK! "+bottleList.get(number).getName()+" tipahti masiinasta!");
    		return 1;
    	}
    }

    public void returnMoney() {
    //Palauttaa loput rahat
    	String change = String.format("%.02f", money);
    	money = 0;
        System.out.println("Klink klink. Sinne meniv�t rahat! Rahaa tuli ulos "+change+"�");
    }
}